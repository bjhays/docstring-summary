# import glob
import os
import fnmatch
import importlib
import inspect
import pprint 
import json

pp = pprint.PrettyPrinter(indent=4)


def process_funcs(l, mod):
	ret = {}
	for test in l:
		if not test.startswith("_"): 
			obj = getattr( mod, test)
			doc_str = inspect.getdoc(obj)
			# print test, obj, doc_str
			try:
				summary = doc_str.split('\n', 1)[0]
			except AttributeError:
				summary = None
			
			ret[test] = {
				"doc_str": doc_str,
				"function_summary": summary
				}
	return ret

def handle_class(obj):
	tmp = {}
	class_doc_str = inspect.getdoc(obj)
	tmp["doc_str"] = class_doc_str
	tmp["class_summary"] = class_doc_str.split('\n', 1)[0]
	members = []
	for m in inspect.getmembers(obj, predicate=inspect.ismethod):
		members.append(m[0])
	tmp["functions"] = process_funcs(members, obj)
	return tmp


files = []
for root, dirnames, filenames in os.walk('project'):
    for filename in fnmatch.filter(filenames, '*.py'):
        files.append(os.path.join(root, filename))

# files = glob.glob("project/*.py", recursive=True)

test_attrs = {
	"class_summary": None,
	"class_doc_str": None,
	"mod_summary": None,
	"mod_doc_str": None,
	"function_summary": None,
	"doc_str": None,
	"functions": None
	}

tests = {}
for f in files:
	print f
	path, name = f.rsplit('/', 1)
	name = name[:-3]
	if name != "__init__":
		module = path.replace("/", ".") + "." + name
		print "Processing {}".format(module)
		mod = importlib.import_module(module)

		for test in dir(mod):
			if not test.startswith("_"):
				tmp = dict(test_attrs)
				print "Processing {}:{}".format(module, test)
				
				obj = getattr( mod, test)
				# print obj, inspect.isclass(obj), callable(obj)
				if inspect.isclass(obj):
					#class
					tmp = handle_class(obj)
				
				elif callable(obj):
					# function
					func_doc_str = inspect.getdoc(obj)					
					tmp["function_summary"] = func_doc_str.split('\n', 1)[0]
					tmp['doc_str'] = func_doc_str

				else:
					pass
					# print "invlid type", obj

				if tmp != test_attrs:
					try:
						mod_doc_str = inspect.getdoc(mod)
						tmp['mod_summary'] = mod_doc_str.split('\n', 1)[0]
						tmp['mod_doc_str'] = mod_doc_str
					except:
						print " - {} missing docstring".format(module)
					tests[test] = tmp

with open("results.json", "w") as f:
	f.write(json.dumps(tests))
