def module_level_function2(param1, param2=None, *args, **kwargs):
    """module_level_function2 description

    Args:
        param1 (int): The first parameter.
        param2 (Optional[str]): The second parameter. Defaults to None.
            Second line of description should be indented.
        *args: Variable length argument list.
        **kwargs: Arbitrary keyword arguments.

    Returns:
        bool: True if successful, False otherwise.

    """
    if param1 == param2:
        raise ValueError('param1 may not be equal to param2')
    return True